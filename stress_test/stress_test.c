#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>

typedef float v4sf __attribute__ ((vector_size(16))); // 4 floats * sizeof(float)=4*4=16

typedef union {
  v4sf v;
  float f[4];
} v4sf_t;

int main()
{
  v4sf_t a, b, c;
  int i,j;
  struct timeval tst, tfin;
  double t;

  a.f[0] = 1; a.f[1] = 2; a.f[2] = 3; a.f[3] = 4;
  b.f[0] = 5; b.f[1] = 6; b.f[2] = 7; b.f[3] = 8;

  gettimeofday(&tst, NULL);
  for (i=0; i != 0xFFFFFFF ; ++i)
    c.v = a.v * b.v;
  gettimeofday(&tfin, NULL);
  t = (tfin.tv_sec - tst.tv_sec) + (double)(tfin.tv_usec - tst.tv_usec)/1e6;
  printf("%f, %f, %f, %f\n", c.f[0], c.f[1], c.f[2], c.f[3]);
  printf("Time taken for SSE: %fs\n",t);

  gettimeofday(&tst, NULL);
  for (i=0; i != 0xFFFFFFF ; ++i)
    for(j = 0; j < 4; ++j)
      c.f[j] = a.f[j] * b.f[j];
  gettimeofday(&tfin, NULL);
  t = (tfin.tv_sec - tst.tv_sec) + (double)(tfin.tv_usec - tst.tv_usec)/1e6;
  printf("%f, %f, %f, %f\n", c.f[0], c.f[1], c.f[2], c.f[3]);
  printf("Time taken for SSE: %fs\n",t);

  return 0;
}
