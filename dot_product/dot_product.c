#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>

#define VEC_LEN 1000
#define REPEAT 1000000

void scalar_dot_product(float x[VEC_LEN], float y[VEC_LEN]);
void sse_dot_product(float x[VEC_LEN], float y[VEC_LEN]);

int main()
{
  float x[VEC_LEN]; float y[VEC_LEN];
  int i;

  struct timeval tst, tfin;
  double t;

  for (i=0; i<VEC_LEN; ++i) {
    x[i] = 0.1;
    y[i] = 0.1;
  }

  gettimeofday(&tst, NULL);
  scalar_dot_product(x, y);
  gettimeofday(&tfin, NULL);
  t = (tfin.tv_sec - tst.tv_sec) + (double)(tfin.tv_usec - tst.tv_usec)/1e6;
  printf("Time taken for scalar: %fs\n",t);

  gettimeofday(&tst, NULL);
  sse_dot_product(x, y);
  gettimeofday(&tfin, NULL);
  t = (tfin.tv_sec - tst.tv_sec) + (double)(tfin.tv_usec - tst.tv_usec)/1e6;
  printf("Time taken for SSE: %fs\n",t);
}


void scalar_dot_product(float x[VEC_LEN], float y[VEC_LEN])
{
  float inner_product = 0.0;
  float result;
  int i, j;

  result = 0.0;
  for (j=0; j<REPEAT; ++j) {
    inner_product = 0.0;
    for (i = 0; i < VEC_LEN; i++) {
      inner_product += x[i] * y[i];
    }
    result += inner_product;
  }
  printf("result=%f\n", result);
}


void sse_dot_product(float x[VEC_LEN], float y[VEC_LEN])
{
  // floating point vector type
  typedef float v4sf __attribute__ ((vector_size (4*sizeof(float))));
  float inner_product = 0.0, temp[4];
  float result;
  v4sf acc, X, Y; // 4x32-bit float registers
  int i, j;

  result = 0.0;
  for (j=0; j<REPEAT; ++j) {
    acc = __builtin_ia32_xorps(acc, acc); // zero the accumulator
    for (i = 0; i < (VEC_LEN - 3); i += 4) {
      X = __builtin_ia32_loadups(&x[i]); // load groups of four floats
      Y = __builtin_ia32_loadups(&y[i]);
      acc = __builtin_ia32_addps(acc, __builtin_ia32_mulps(X, Y));
    }
    __builtin_ia32_storeups(temp, acc); // add the accumulated values

    inner_product = temp[0] + temp[1] + temp[2] + temp[3];
    for (; i < VEC_LEN; i++) // add up the remaining floats
      inner_product += x[i] * y[i];
    result += inner_product;
  }
  printf("result=%f\n", result);
}
