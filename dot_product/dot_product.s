	.file	"dot_product.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"result=%f\n"
	.text
	.p2align 4,,15
	.globl	scalar_dot_product
	.type	scalar_dot_product, @function
scalar_dot_product:
.LFB12:
	.cfi_startproc
	xorps	%xmm3, %xmm3
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2:
	xorps	%xmm1, %xmm1
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L3:
	movss	(%rsi,%rax), %xmm2
	shufps	$0, %xmm2, %xmm2
	movss	(%rdi,%rax), %xmm4
	shufps	$0, %xmm4, %xmm4
	addq	$4, %rax
	cmpq	$4000, %rax
	movaps	%xmm2, %xmm0
	mulps	%xmm4, %xmm0
	addps	%xmm0, %xmm1
	jne	.L3
	addl	$1, %edx
	addps	%xmm1, %xmm3
	cmpl	$250000, %edx
	jne	.L2
	haddps	%xmm3, %xmm3
	movl	$.LC0, %edi
	movl	$1, %eax
	haddps	%xmm3, %xmm3
	movaps	%xmm3, %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	jmp	printf
	.cfi_endproc
.LFE12:
	.size	scalar_dot_product, .-scalar_dot_product
	.p2align 4,,15
	.globl	sse_dot_product
	.type	sse_dot_product, @function
sse_dot_product:
.LFB13:
	.cfi_startproc
	xorps	%xmm3, %xmm3
	subq	$24, %rsp
	.cfi_def_cfa_offset 32
	movl	$1000000, %edx
	.p2align 4,,10
	.p2align 3
.L9:
	xorps	%xmm1, %xmm1
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L8:
	movups	(%rdi,%rax), %xmm0
	movups	(%rsi,%rax), %xmm2
	addq	$16, %rax
	cmpq	$4000, %rax
	mulps	%xmm2, %xmm0
	addps	%xmm0, %xmm1
	jne	.L8
	movups	%xmm1, (%rsp)
	subl	$1, %edx
	movss	4(%rsp), %xmm0
	addss	(%rsp), %xmm0
	addss	8(%rsp), %xmm0
	addss	12(%rsp), %xmm0
	addss	%xmm0, %xmm3
	jne	.L9
	unpcklps	%xmm3, %xmm3
	movl	$.LC0, %edi
	movl	$1, %eax
	cvtps2pd	%xmm3, %xmm0
	call	printf
	addq	$24, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE13:
	.size	sse_dot_product, .-sse_dot_product
	.section	.rodata.str1.1
.LC4:
	.string	"Time taken for scalar: %fs\n"
.LC5:
	.string	"Time taken for SSE: %fs\n"
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB11:
	.cfi_startproc
	subq	$8040, %rsp
	.cfi_def_cfa_offset 8048
	xorl	%eax, %eax
	movaps	.LC2(%rip), %xmm0
	.p2align 4,,10
	.p2align 3
.L14:
	movaps	%xmm0, 32(%rsp,%rax)
	movaps	%xmm0, 4032(%rsp,%rax)
	addq	$16, %rax
	cmpq	$4000, %rax
	jne	.L14
	movq	%rsp, %rdi
	xorl	%esi, %esi
	call	gettimeofday
	leaq	4032(%rsp), %rsi
	leaq	32(%rsp), %rdi
	call	scalar_dot_product
	leaq	16(%rsp), %rdi
	xorl	%esi, %esi
	call	gettimeofday
	movq	24(%rsp), %rax
	subq	8(%rsp), %rax
	movl	$.LC4, %edi
	cvtsi2sdq	%rax, %xmm0
	movq	16(%rsp), %rax
	subq	(%rsp), %rax
	cvtsi2sdq	%rax, %xmm1
	movl	$1, %eax
	mulsd	.LC3(%rip), %xmm0
	addsd	%xmm1, %xmm0
	call	printf
	movq	%rsp, %rdi
	xorl	%esi, %esi
	call	gettimeofday
	leaq	4032(%rsp), %rsi
	leaq	32(%rsp), %rdi
	call	sse_dot_product
	leaq	16(%rsp), %rdi
	xorl	%esi, %esi
	call	gettimeofday
	movq	24(%rsp), %rax
	subq	8(%rsp), %rax
	movl	$.LC5, %edi
	cvtsi2sdq	%rax, %xmm0
	movq	16(%rsp), %rax
	subq	(%rsp), %rax
	cvtsi2sdq	%rax, %xmm1
	movl	$1, %eax
	mulsd	.LC3(%rip), %xmm0
	addsd	%xmm1, %xmm0
	call	printf
	addq	$8040, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE11:
	.size	main, .-main
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	1036831949
	.long	1036831949
	.long	1036831949
	.long	1036831949
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	2696277389
	.long	1051772663
	.ident	"GCC: (SUSE Linux) 4.7.2 20130108 [gcc-4_7-branch revision 195012]"
	.section	.comment.SUSE.OPTs,"MS",@progbits,1
	.string	"Ospwg"
	.section	.note.GNU-stack,"",@progbits
