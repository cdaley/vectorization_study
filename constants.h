#ifndef CONSTANTS_H
#define CONSTANTS_H

#define FLOPS_ARRAY_SIZE (1024*1024)

#ifdef __MIC__
# define MAXFLOPS_ITERS 20000000
#else
# define MAXFLOPS_ITERS 1000000
#endif

#define LOOP_COUNT 128

// number of float pt ops per calculation
#define FLOPSPERCALC 2

#endif
