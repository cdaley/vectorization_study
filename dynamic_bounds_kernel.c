#include "dynamic_bounds_kernel.h"

void dynamic_bounds_kernel(float * const x,
			   const float * const y,
			   const float a,
			   const int iters,
			   const int cnt)
{
  int j, k;

  // loop many times to get lots of calculations
  for(j=0; j<iters; j++)
  {
    // scale 1st array and add in the 2nd array
#pragma loop count(LOOP_COUNT)
#pragma unroll(8)
#pragma simd
#pragma vector aligned
    for(k=0; k<cnt; k++)
    {
      x[k] = a * x[k] + y[k];
    }
  }
}
