#ifndef DYNAMIC_BOUNDS_KERNEL_H
#define DYNAMIC_BOUNDS_KERNEL_H

#include "constants.h"

void dynamic_bounds_kernel(float * const x,
			   const float * const y,
			   const float a,
			   const int iters,
			   const int cnt);
#endif
