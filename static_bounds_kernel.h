#ifndef STATIC_BOUNDS_KERNEL_H
#define STATIC_BOUNDS_KERNEL_H

#include "constants.h"

typedef float* __restrict__  __attribute__((align_value (64))) float_ptr;

void static_bounds_kernel(float * const x,
			  const float * const y,
			  const float a);

void static_bounds_kernel_value_att(float_ptr const x,
				    const float_ptr const y,
				    const float a);

#endif
