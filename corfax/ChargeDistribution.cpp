#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#ifdef __GNUC__
# define _mm_malloc(x,y) malloc(x)
# define _mm_free(x) free(x)
# define restrict __restrict__
#endif

class Charge_Distribution{
public:
  int m; // Number of charges
  float *x, *y, *z; // Arrays of coordinates of charges
  float *q; // Array of charge values

  Charge_Distribution(const int M) : m(M) {
    x = (float *) _mm_malloc(m*sizeof(float), 32);
    y = (float *) _mm_malloc(m*sizeof(float), 32);
    z = (float *) _mm_malloc(m*sizeof(float), 32);
    q = (float *) _mm_malloc(m*sizeof(float), 32);
  }

  Charge_Distribution() {
    _mm_free(x);
    _mm_free(y);
    _mm_free(z);
    _mm_free(q);
  }
};


void calc_potential(
  const int m, // Number of charges
  const int n, // Number of points in xy-plane in each dimension
  const Charge_Distribution &chg, // Charge distribution
  float * restrict const phi, // electrical potential
  const float ds // Spatial grid spacing
  ) {
  for (int c=0; c<n*n; c++) {
    const float Rx = ds * (float)(c / n);
    const float Ry = ds * (float)(c % n);
    const float Rz = ds * (float)(0);
    //printf("c=%d, Rx=%f, Ry=%f, Rz=%f\n", c, Rx, Ry, Rz);
#pragma vector aligned
    for (int i=0; i<m; i++) {
      const float dx = chg.x[i] - Rx;
      const float dy = chg.y[i] - Ry;
      const float dz = chg.z[i] - Rz;
      //printf("dx=%f, dy=%f, dz=%f\n", dx, dy, dz);
      phi[c] -= chg.q[i] / sqrtf(dx*dx + dy*dy + dz*dz);
      //printf("phi[%d]=%f\n", c, phi[c]);
    }
    //printf("\n");
  }
}


void init_charge_array(
  const int m,
  Charge_Distribution &chg
  ) {
  int loc = m / 2;
  for (int i=0; i<m; i++) {
    chg.x[i] = (float) loc;
    chg.y[i] = (float) i;
    chg.z[i] = (float) i;
    chg.q[i] = (float) i;
  }
}

int main()
{
  //int n = 512, m = 128;
  int n = 1000, m = 1000;
  float *phi = (float*) _mm_malloc(n*n*sizeof(float), 32);
  Charge_Distribution * CD = new Charge_Distribution(m);
  init_charge_array(m, *CD);
  calc_potential(m, n, *CD, phi, 1.0f / (float)(n-1));
  printf("Output = %f\n", phi[0]);
}
