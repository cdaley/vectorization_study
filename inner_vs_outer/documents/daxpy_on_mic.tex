\documentclass{beamer}
\mode<presentation>

\usetheme{Copenhagen}

\useoutertheme[subsection=false]{smoothbars}

%gets rid of bottom navigation bars - replaces with page number
\setbeamertemplate{footline}[page number]{}

%gets rid of navigation symbols
\setbeamertemplate{navigation symbols}{}

\setbeamertemplate{caption}[numbered]

\usepackage{graphicx}
\usepackage{subfig}
\usepackage{listings}



\title{Performance on Edison and Babbage}
%\subtitle{...}
\author{Christopher Daley}
\date{November 14, 2013}


\begin{document}

%\frame{
%	\titlepage
%}

%\section[Outline]{}
%\frame{\tableofcontents}

%\section{Introduction}

%\section{Performance}


\frame{\frametitle{Introduction}
  \begin{itemize}
  \item Found that the performance of a simple DAXPY kernel on the
    Intel Xeon-Phi is sensitive to
    \begin{enumerate}
    \item The type of memory allocation - dynamic or static
    \item The location of the DAXPY kernel - in a function in the same
      source file as the memory allocation or in a different source file
    \item The size of the arrays, even though the arrays should always fit
      in L1 cache
    \end{enumerate}
  \item The code is an extension of the ``helloflops3'' C code from
    the ``Intel Xeon Phi Coprocessor High Performance Programming''
    book written by Jim Jeffers
  \end{itemize}
}


\defverbatim[colored]\DAXPY{%
\begin{lstlisting}[showspaces=false, showstringspaces=false,
  showtabs=false, language=C, basicstyle=\tiny]
typedef double real;

void static_bounds_kernel(real * x, real * y, real a)
{
  int j, k;
#if defined(__IBMC__) || defined(__IBMCPP__)
#pragma disjoint(*x, *y)
  __alignx(32, x);
  __alignx(32, y);
#endif

  // loop many times to get lots of calculations
  for(j=0; j<MAXFLOPS_ITERS; j++)
  {
    // scale 1st array and add in the 2nd array
#if defined(__ICC) || defined(__INTEL_COMPILER)
#pragma simd
#pragma vector aligned
#elif defined(__IBMC__) || defined(__IBMCPP__)
#pragma ibm independent_loop
#endif
    for(k=0; k<LOOP_COUNT; k++)
    {
      x[k] = a * x[k] + y[k];
    }
  }
}
\end{lstlisting}
}


\frame{\frametitle{DAXPY kernel function}
\DAXPY
}


\frame{\frametitle{Experiments}
  \begin{itemize}
  \item The base compiler flags used on the Intel Xeon-Phi are
    "-openmp -O3 -mmic''
  \item All graphs show the performance of the DAXPY kernel for
    \begin{itemize}
    \item Arrays of 32, 128 and 512 elements
    \item Kernels that are either explicitly inlined or in a separate
      function.  The separate functions
      \begin{itemize}
      \item Are either in the same source file as the memory
        allocation or in a different source file
      \item May or may not use the \texttt{\_\_restrict\_\_} keyword for the
        arguments *x and *y
      \end{itemize}
    \end{itemize}
  \item There are 4 graphs per slide for
    \begin{itemize}
      \item Floats and doubles
      \item Static and dynamic memory allocation
    \end{itemize}
  \item The time spent in each DAXPY kernel is at least 1.5 seconds
  \end{itemize}
}


\frame{\frametitle{Babbage, 122 threads}
  \begin{figure}
    \centering
    \vspace*{-0.7cm}
    \subfloat[Static and double]{\includegraphics[width=.35\textwidth]{../figures/babbage/O3/babbage_static_double.png}}
    \subfloat[Dynamic and double]{\includegraphics[width=.35\textwidth]{../figures/babbage/O3/babbage_dynamic_double.png}}\\
    \vspace*{-0.3cm}
    \subfloat[Static and float]{\includegraphics[width=.35\textwidth]{../figures/babbage/O3/babbage_static_float.png}}
    \subfloat[Dynamic and float]{\includegraphics[width=.35\textwidth]{../figures/babbage/O3/babbage_dynamic_float.png}}
    \caption{}
    \label{fig:babbage_O3}
  \end{figure}
}


\frame{\frametitle{Concerns}
  \begin{enumerate}
  \item The performance changes quite dramatically with arrays of
    different size even though
    \begin{itemize}
    \item The computational intensity of the kernel remains the
      same
    \item The arrays should fit in L1 cache
    \end{itemize}
  \item For static allocation, the performance is worse in one
    situation
    \begin{itemize}
      \item Kernel is in a different file and does not have
        the \texttt{\_\_restrict\_\_} keyword
    \end{itemize}
  \item For dynamic allocation, the performance is worse in three
    situations
    \begin{itemize}
      \item Kernel is explicitly inlined
      \item Kernel is in the same file and does not have the
        \texttt{\_\_restrict\_\_} keyword
      \item Kernel is in a different file and does not have the
        \texttt{\_\_restrict\_\_} keyword
    \end{itemize}
  \end{enumerate}
}


\frame{\frametitle{Overview}
  \begin{itemize}
  \item I will ignore the performance differences due to array size
    \begin{itemize}
      \item It is likely that the quality of the compiler generated
        instruction scheduling and register allocation changes with array
        size
    \end{itemize}
  \item I will investigate the performance differences due to the 
    \texttt{\_\_restrict\_\_} keyword and the type of memory allocation
    \begin{itemize}
    \item The \texttt{\_\_restrict\_\_} keyword should not be needed
      because we already assert that there are no loop dependencies by
      insertion of \texttt{\#pragma simd}
    \item The vectorization report generated with
      \texttt{-vec-report=6} is identical with and without the
      \texttt{\_\_restrict\_\_} keyword
    \end{itemize}
  \item The next slide shows how the performance changes when adding
    \texttt{-ipo} to the base flags
  \end{itemize}
}



\frame{\frametitle{Babbage, 122 threads, with -ipo}
  \begin{figure}
    \centering
    \vspace*{-0.7cm}
    \subfloat[Static and double]{\includegraphics[width=.35\textwidth]{../figures/babbage/O3_ipo/babbage_static_double.png}}
    \subfloat[Dynamic and double]{\includegraphics[width=.35\textwidth]{../figures/babbage/O3_ipo/babbage_dynamic_double.png}}\\
    \vspace*{-0.3cm}
    \subfloat[Static and float]{\includegraphics[width=.35\textwidth]{../figures/babbage/O3_ipo/babbage_static_float.png}}
    \subfloat[Dynamic and float]{\includegraphics[width=.35\textwidth]{../figures/babbage/O3_ipo/babbage_dynamic_float.png}}
    \caption{}
    \label{fig:babbage_O3_ipo}
  \end{figure}
}



\frame{\frametitle{Effect of -ipo}
  \begin{itemize}
  \item Compiling with \texttt{-ipo} gives consistent performance with
    one exception
    \begin{itemize}
    \item The performance of the explicitly inlined kernel
      with dynamically allocated arrays
      \begin{itemize}
      \item This will be ignored because the main function is already quite
        complicated so it is unsurprising the generated code is worse
      \end{itemize}
    \end{itemize}
  \item It is strange that \texttt{-ipo} helps
    \begin{itemize}
      \item Inlining should give no benefit since the overhead of the
        function call is negligible compared to the amount of work in
        the function.  Something else is happening!
    \end{itemize}
  \item Since the \texttt{\_\_restrict\_\_} keyword helps performance
    when not using \texttt{-ipo}, do aliasing compiler options help
    the performance too?
    \begin{itemize}
    \item This is of interest because many apps do not use
      \texttt{-ipo} because of slow compile times.  Also it may not be
      possible to get the same quality of global analysis in a real
      app
    \end{itemize}
  \end{itemize}
}


\frame{\frametitle{Babbage, 122 threads, with -ansi-alias -fno-fnalias}
  \begin{figure}
    \centering
    \vspace*{-0.7cm}
    \subfloat[Static and double]{\includegraphics[width=.35\textwidth]{../figures/babbage/O3_fn_alias/babbage_static_double.png}}
    \subfloat[Dynamic and double]{\includegraphics[width=.35\textwidth]{../figures/babbage/O3_fn_alias/babbage_dynamic_double.png}}\\
    \vspace*{-0.3cm}
    \subfloat[Static and float]{\includegraphics[width=.35\textwidth]{../figures/babbage/O3_fn_alias/babbage_static_float.png}}
    \subfloat[Dynamic and float]{\includegraphics[width=.35\textwidth]{../figures/babbage/O3_fn_alias/babbage_dynamic_float.png}}
    \caption{}
    \label{fig:babbage_O3_fn_alias}
  \end{figure}
}


\frame{\frametitle{Aliasing options}
  \begin{itemize}
  \item Yes, we can get the same performance as \texttt{-ipo} when using
    \begin{enumerate}
    \item -fno-alias
      \begin{itemize}
      \item This is too dangerous to use in most applications
      \end{itemize}
    \item -ansi-alias -fno-fnalias
      \begin{itemize}
      \item Should be safe for well written applications
      \end{itemize}
    \end{enumerate}
  \item The following aliasing flags fail to improve performance to
    the same level as \texttt{-ipo}
    \begin{enumerate}
    \item -ansi-alias -fargument-noalias
    \item -ansi-alias -fargument-noalias-global
    \end{enumerate}
  \item The aliasing options are not needed on Edison.  The next
    slides show \texttt{-O3 -no-ipo} gives the same performance as
    \texttt{-fast} (which includes \texttt{-ipo} option) on Edison
  \end{itemize}
}


\frame{\frametitle{Edison,  24 threads,  -O3 -no-ipo}
  \begin{figure}
    \centering
    \vspace*{-0.7cm}
    \subfloat[Static and double]{\includegraphics[width=.35\textwidth]{../figures/edison/O3_no-ipo/edison_static_double.png}}
    \subfloat[Dynamic and double]{\includegraphics[width=.35\textwidth]{../figures/edison/O3_no-ipo/edison_dynamic_double.png}}\\
    \vspace*{-0.3cm}
    \subfloat[Static and float]{\includegraphics[width=.35\textwidth]{../figures/edison/O3_no-ipo/edison_static_float.png}}
    \subfloat[Dynamic and float]{\includegraphics[width=.35\textwidth]{../figures/edison/O3_no-ipo/edison_dynamic_float.png}}
    \caption{}
    \label{fig:edison_O3_noipo}
  \end{figure}
}


\frame{\frametitle{Edison, 24 threads, -fast}
  \begin{figure}
    \centering
    \vspace*{-0.7cm}
    \subfloat[Static and double]{\includegraphics[width=.35\textwidth]{../figures/edison/fast/edison_static_double.png}}
    \subfloat[Dynamic and double]{\includegraphics[width=.35\textwidth]{../figures/edison/fast/edison_dynamic_double.png}}\\
    \vspace*{-0.3cm}
    \subfloat[Static and float]{\includegraphics[width=.35\textwidth]{../figures/edison/fast/edison_static_float.png}}
    \subfloat[Dynamic and float]{\includegraphics[width=.35\textwidth]{../figures/edison/fast/edison_dynamic_float.png}}
    \caption{}
    \label{fig:edison_fast}
  \end{figure}
}


\frame{\frametitle{Conclusion}
  \begin{itemize}
  \item Intel compiler pragmas alone are not enough to get the best
    possible performance on the Intel Xeon-Phi
  \item Must also use the \texttt{\_\_restrict\_\_} keyword or
    aliasing compiler options
    \begin{itemize}
    \item If neither is present
      \begin{itemize}
      \item The vectorization report \texttt{-vec-report=6} does not
        show that the performance will be compromised
      \item Only the verbose (and hard to read) optimization reports
        \texttt{-opt-report=2} and \texttt{-opt-report=3} tell us that
        there is a possible data dependency
      \end{itemize}
    \end{itemize}
  \item Recommended flags for application codes on the Intel Xeon-Phi are 
    \texttt{-openmp -O3 -mmic -ansi-alias -fno-fnalias}
  \end{itemize}
}


%< <test.c;19:19;hlo_linear_trans;static_bounds_kernel;0>
%< Loop Interchange not done due to: Data Dependencies
%<   Dependencies found between following statements:
%<     [From_Line# -> (Dependency Type) To_Line#]
%<     [30 ->(Flow) 30] [30 ->(Anti) 30] 
%< Advice: Loop Interchange, if possible, might help Loopnest at lines: 19 28 
%<       : Suggested Permutation: (1 2 ) --> ( 2 1 )



\frame{\frametitle{Mira, 32 threads, -qnoipa}
  \begin{figure}
    \centering
    \vspace*{-0.7cm}
    \subfloat[Static and double]{\includegraphics[width=.35\textwidth]{../figures/mira/qnoipa/mira_static_double.png}}
    \subfloat[Dynamic and double]{\includegraphics[width=.35\textwidth]{../figures/mira/qnoipa/mira_dynamic_double.png}}\\
    \vspace*{-0.3cm}
    \subfloat[Static and float]{\includegraphics[width=.35\textwidth]{../figures/mira/qnoipa/mira_static_float.png}}
    \subfloat[Dynamic and float]{\includegraphics[width=.35\textwidth]{../figures/mira/qnoipa/mira_dynamic_float.png}}
    \caption{}
    \label{fig:mira_qnoipa}
  \end{figure}
}


\frame{\frametitle{Mira, 32 threads, -qipa=level=2}
  \begin{figure}
    \centering
    \vspace*{-0.7cm}
    \subfloat[Static and double]{\includegraphics[width=.35\textwidth]{../figures/mira/qipa_level_2/mira_static_double.png}}
    \subfloat[Dynamic and double]{\includegraphics[width=.35\textwidth]{../figures/mira/qipa_level_2/mira_dynamic_double.png}}\\
    \vspace*{-0.3cm}
    \subfloat[Static and float]{\includegraphics[width=.35\textwidth]{../figures/mira/qipa_level_2/mira_static_float.png}}
    \subfloat[Dynamic and float]{\includegraphics[width=.35\textwidth]{../figures/mira/qipa_level_2/mira_dynamic_float.png}}
    \caption{}
    \label{fig:mira_qipa_level_2}
  \end{figure}
}

%\section*{}

%\frame{
%    \begin{center}
%      Any questions?
%    \end{center}
%}


%\appendix
%\section*{Backup slides}

%\frame{\frametitle{Placeholder}
%  HI
%}

\end{document}


\frame{\frametitle{Babbage, 122 threads, -O3 and alias options and ivdep}
  \begin{figure}
    \centering
    \vspace*{-0.7cm}
    \subfloat[Static and double]{\includegraphics[width=.35\textwidth]{../figures/babbage/O3_alias_options_ivdep/babbage_static_double.png}}
    \subfloat[Dynamic and double]{\includegraphics[width=.35\textwidth]{../figures/babbage/O3_alias_options_ivdep/babbage_dynamic_double.png}}\\
    \vspace*{-0.3cm}
    \subfloat[Static and float]{\includegraphics[width=.35\textwidth]{../figures/babbage/O3_alias_options_ivdep/babbage_static_float.png}}
    \subfloat[Dynamic and float]{\includegraphics[width=.35\textwidth]{../figures/babbage/O3_alias_options_ivdep/babbage_dynamic_float.png}}
    \caption{}
    \label{fig:babbage_O3_alias_ivdep}
  \end{figure}
}
