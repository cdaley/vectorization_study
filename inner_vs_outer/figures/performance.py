#!/usr/bin/env python
from matplotlib import pyplot as plt
import numpy as np
import matplotlib.backends.backend_pdf


class perf_output:
    def __init__(self, filename):
        self.time = {}
        self.perf_data = {'static': {'float': {}, 'double': {}},
                          'dynamic': {'float': {}, 'double': {}}}
        self.read(filename)

    def kernel_data(self, alloc, flttype, kernel):
        x = self.perf_data[alloc][flttype]
        return np.array([(i,x[i][kernel]) for i in x if i <= 512])

    def kernel_dict(self, alloc, flttype, kernel):
        x = self.perf_data[alloc][flttype]
        return {k : x[k][kernel] for k in x if k <= 512}

    def read(self, filename):
        f = open(filename)
        filelines = f.readlines()
        f.close()

        numlines = len(filelines)
        startlines = []
        for i,l in enumerate(filelines):
            if 'Arrays are ' in l:
                startlines.append(i)

        endlines = [x-1 for x in startlines]
        endlines.pop(0)
        endlines.append(numlines-1)
        exp_ptrs = zip(startlines, endlines)
        for s,e in exp_ptrs:
            self.parse_experiment(filelines[s:e])

    def parse_experiment(self, lines):
        alloc = 'static' if 'Arrays are statically allocated' in lines[0] else 'dynamic'
        flttype = 'float' if 'Floating point size = 4' in lines[1] else 'double'
        innerloop = int(lines[2].split()[-1])
        kernels = [float(lines[x].split()[-1]) for x in range(11, 31+1, 4)]

        # Add the kernel list to the performance data dictionary
        self.perf_data[alloc][flttype][innerloop] = kernels



class perf_summary:
    def __init__(self, filenames):
        self.perf_coll = [perf_output(x) for x in filenames]

    def kernel_data(self, alloc, flttype, kernel):
        dicts = [x.kernel_dict(alloc, flttype, kernel) for x in self.perf_coll]

        super_dict = {}
        for k in set(k for d in dicts for k in d):
            super_dict[k] = [d[k] for d in dicts if k in d]

        data = []
        for k in super_dict:
            values = np.array(super_dict[k])
            count = len(values)
            mean = np.mean(values, dtype=np.float64)
            stddev = np.std(values, ddof=1, dtype=np.float64)
            stderr = 1.96 * (stddev / np.sqrt(count, dtype=np.float64)) # 95% interval
            data.append((k, mean, stderr))
        return np.array(data)


def percent_fp_peak(data, machine, flttype):
    if machine == 'edison':
        # 256-bit SIMD unit with AVX instructions
        clk = 2.4 # GHz
        cores = 24
        if flttype == 'double':
            peak = cores * clk * 4 * 2 # 4 DPs * 2 FMA
        else:
            peak = cores * clk * 8 * 2
    elif machine == 'babbage':
        # 512-bit SIMD unit with Initial Many Core Intructions (IMCI) instructions
        clk = 1.091 # GHz
        cores = 61
        if flttype == 'double':
            peak = cores * clk * 8 * 2 # 8 DPs * 2 FMA
        else:
            peak = cores * clk * 16 * 2
    elif machine == 'mira':
        # Quad SIMD unit with QPX instructions
        # L1: 16KB data: 64 byte line, 32 byte load/store interface, 6 cycle latency
        # L1: 16KB instruction: 3 cycle latency
        clk = 1.6
        cores = 16
        # I think all QPX instructions are double precision       
        peak = cores * clk * 4 * 2 # 4 DPs * 2 FMA

    # Scale the mean and (optional) standard error to give percentage of peak
    if data.shape[1] == 2:
        ret = np.array([(i,(j*100.0)/peak) for i,j in data])
    else:
        ret = np.array([(i,(j*100.0)/peak,(k*100.0)/peak) for i,j,k in data])
    return ret



plt.rcParams['font.sans-serif'] = 'FreeSans'
plt.rcParams['font.size'] = 20
plt.rcParams['font.weight'] = 'bold'
plt.rcParams['legend.fontsize'] = 16
plt.rcParams['legend.handlelength'] = 4.0
plt.rcParams['legend.numpoints'] = 1
plt.rcParams['xtick.labelsize'] = 18
plt.rcParams['ytick.labelsize'] = 18


#machine = 'edison'
#filedir = '../data/edison/normal/'
#filenames = [filedir + x for x in ['edison.59b4e2f', 'edison.31d95d6', 'edison.e601da8']]
#summary = perf_summary(filenames)
#filename = filedir + 'dbgjob.o513849' # -O3
#filename = filedir + 'dbgjob.o513873' # -fast
#filename = filedir + 'dbgjob.o517037' # -O3 -no-ipo
#summary = perf_output(filename) # for a single run

#machine = 'mira'
#filedir = '../data/mira/'
#filename = filedir + 'mira.qnoipa' # -qnoipa
#filename = filedir + 'mira.qipa_level_0' # -qipa=level=0
#filename = filedir + 'mira.qipa_level_2' # -qipa=level=2
#summary = perf_output(filename) # for a single run

machine = 'babbage'
filedir = '../data/babbage/'
#filename = filedir + 'test_vectorization.o5509' # -O3
#filename = filedir + 'test_vectorization.o5515' # -O3 -ipo
#filename = filedir + 'test_vectorization.o5516' # -O3 -no-ipo -ansi-alias -fargument-noalias
#filename = filedir + 'test_vectorization.o5518' # -O3 -no-ipo -ansi-alias -fargument-noalias AND ivdep #NOT USEFUL
#filename = filedir + 'test_vectorization.o5523' # -O3 -no-ipo -ansi-alias -fargument-noalias-global
#filename = filedir + 'test_vectorization.o5524' # -O3 -no-ipo -fno-alias
filename = filedir + 'test_vectorization.o5525' # -O3 -no-ipo -ansi-alias -fno-fnalias
summary = perf_output(filename) # for a single run

plotFPPeak = True
plotErrorBars = False

for alloc in ['static', 'dynamic']:
    for flttype in ['float', 'double']:
        image_name = machine + '_' + alloc + '_' + flttype + '.png'

        kernel0 = summary.kernel_data(alloc, flttype, 0)
        kernel1 = summary.kernel_data(alloc, flttype, 1)
        kernel2 = summary.kernel_data(alloc, flttype, 2)
        kernel4 = summary.kernel_data(alloc, flttype, 4)
        kernel5 = summary.kernel_data(alloc, flttype, 5)

        if plotFPPeak:
            kernel0 = percent_fp_peak(kernel0, machine, flttype)
            kernel1 = percent_fp_peak(kernel1, machine, flttype)
            kernel2 = percent_fp_peak(kernel2, machine, flttype)
            kernel4 = percent_fp_peak(kernel4, machine, flttype)
            kernel5 = percent_fp_peak(kernel5, machine, flttype)

        fig = plt.figure()
        ax = fig.add_subplot(111)

        ax.plot(kernel0[:,0],kernel0[:,1], 'r*-', ms=18, lw=1.5, label='Explicitly inlined')
        ax.plot(kernel1[:,0],kernel1[:,1], 'gv-', ms=14, lw=1.5, label='Same file')
        ax.plot(kernel2[:,0],kernel2[:,1], 'b^-', ms=14, lw=1.5, label='Same file + restrict')
        ax.plot(kernel4[:,0],kernel4[:,1], 'm+-', ms=14, lw=1.5, mew=2.5, label='Diff file')
        ax.plot(kernel5[:,0],kernel5[:,1], 'yx-', ms=14, lw=1.5, mew=2.5, label='Diff file + restrict')
        if plotErrorBars and kernel1.shape[1] > 2:
            ax.errorbar(kernel0[:,0], kernel0[:,1], yerr=kernel1[:,2], color='r', label='', capsize=10, lw=1.5, mew=2)
            ax.errorbar(kernel1[:,0], kernel1[:,1], yerr=kernel2[:,2], color='g', label='', capsize=10, lw=1.5, mew=2)
            ax.errorbar(kernel2[:,0], kernel2[:,1], yerr=kernel3[:,2], color='b', label='', capsize=10, lw=1.5, mew=2)
            ax.errorbar(kernel4[:,0], kernel4[:,1], yerr=kernel5[:,2], color='m', label='', capsize=10, lw=1.5, mew=2)
            ax.errorbar(kernel5[:,0], kernel5[:,1], yerr=kernel6[:,2], color='y', label='', capsize=10, lw=1.5, mew=2)            

        ax.set_xlabel(("Inner loop size"))
        ax.set_xscale('log', basex=2)
        ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        ax.set_xticks([32,128,512])
        ax.set_xticklabels(['32','128','512'])

        # Extend the axis so that the data points do not get cut off
        xl,xu = ax.get_xlim()
        factor = 16.0
        newxl = xl - (xl/factor)
        newxu = xu + (xu/factor)
        ax.set_xlim((newxl,newxu))

        if plotFPPeak:
            ax.set_ylabel(("Percentage of FP peak"))
            ax.set_ylim((0, 100))
        else:
            ax.set_ylabel(("GFLOP / s"))

        ax.legend(loc='upper right')
        ax.grid(True)

        fig.savefig(image_name)
