#!/usr/bin/env python
from matplotlib import pyplot as plt
import numpy as np
import matplotlib.backends.backend_pdf


class perf_output:
    def __init__(self, filename):
        self.time = {}
        self.perf_data = {'static': {'float': {}, 'double': {}},
                          'dynamic': {'float': {}, 'double': {}}}
        self.filelines = self.read(filename)

    def kernel_data(self, alloc, flttype, kernel):
        x = self.perf_data[alloc][flttype]
        loc = kernel - 1
        return np.array([(i,x[i][loc],0.0) for i in x if i <= 512])

    def kernel_dict(self, alloc, flttype, kernel):
        x = self.perf_data[alloc][flttype]
        loc = kernel - 1
        return {k : x[k][loc] for k in x if k <= 512}

    def read(self, filename):
        f = open(filename)
        self.filelines = f.readlines()
        f.close()

        numlines = len(self.filelines)
        startlines = []
        for i,l in enumerate(self.filelines):
            if 'Arrays are ' in l:
                startlines.append(i)

        endlines = [x-1 for x in startlines]
        endlines.pop(0)
        endlines.append(numlines-1)
        exp_ptrs = zip(startlines, endlines)
        for s,e in exp_ptrs:
            self.parse_experiment(self.filelines[s:e])

    def parse_experiment(self, lines):
        alloc = 'static' if 'Arrays are statically allocated' in lines[0] else 'dynamic'
        flttype = 'float' if 'Floating point size = 4' in lines[2] else 'double'
        innerloop = int(lines[3].split()[-1])
        kernels = [float(lines[x].split()[-1]) for x in range(8, 28+1, 4)]

        # Add the kernel list to the performance data dictionary
        self.perf_data[alloc][flttype][innerloop] = kernels



class perf_summary:
    def __init__(self, filenames):
        self.perf_coll = [perf_output(x) for x in filenames]

    def kernel_data(self, alloc, flttype, kernel):
        dicts = [x.kernel_dict(alloc, flttype, kernel) for x in self.perf_coll]

        super_dict = {}
        for k in set(k for d in dicts for k in d):
            super_dict[k] = [d[k] for d in dicts if k in d]

        data = []
        for k in super_dict:
            values = np.array(super_dict[k])
            count = len(values)
            mean = np.mean(values, dtype=np.float64)
            stddev = np.std(values, ddof=1, dtype=np.float64)
            stderr = 1.96 * (stddev / np.sqrt(count, dtype=np.float64)) # 95% interval
            data.append((k, mean, stderr))
        return np.array(data)


def percent_fp_peak(data, machine, flttype):
    if machine == 'edison':
        # 256-bit SIMD unit with AVX instructions
        clk = 2.4 # GHz
        cores = 24
        if flttype == 'double':
            peak = cores * clk * 4 * 2 # 4 DPs * 2 FMA
        else:
            peak = cores * clk * 8 * 2
    elif machine == 'babbage':
        # 512-bit SIMD unit with Initial Many Core Intructions (IMCI) instructions
        clk = 1.091 # GHz
        cores = 61
        if flttype == 'double':
            peak = cores * clk * 8 * 2 # 8 DPs * 2 FMA
        else:
            peak = cores * clk * 16 * 2
    elif machine == 'mira':
        # Quad SIMD unit with QPX instructions
        # L1: 16KB data: 64 byte line, 32 byte load/store interface, 6 cycle latency
        # L1: 16KB instruction: 3 cycle latency
        clk = 1.6
        cores = 16
        if flttype == 'double':
            peak = cores * clk * 4 * 2 # 4 DPs * 2 FMA
        else:
            peak = cores * clk * 8 * 2

    # Scale the mean and standard error to give percentage of peak
    return np.array([(i,(j*100.0)/peak,(k*100.0)/peak) for i,j,k in data])


plt.rcParams['font.sans-serif'] = 'FreeSans'
plt.rcParams['font.size'] = 20
plt.rcParams['font.weight'] = 'bold'
plt.rcParams['legend.fontsize'] = 16
plt.rcParams['legend.handlelength'] = 4.0
plt.rcParams['legend.numpoints'] = 1
plt.rcParams['xtick.labelsize'] = 18
plt.rcParams['ytick.labelsize'] = 18


#machine = 'edison'
#filedir = '../data/edison/normal/'
#filenames = [filedir + x for x in ['edison.59b4e2f', 'edison.31d95d6', 'edison.e601da8']]
#summary = perf_summary(filenames)

machine = 'mira'
filedir = '../../data/mira/'
#filenames = [filedir + x for x in ['mira.62e6768.16']]
summary = perf_output(filedir + 'mira.62e6768.16')
#summary = perf_output(filedir + 'mira.62e6768.32')
#summary = perf_output(filedir + 'mira.62e6768.64')

plotFPPeak = True


for alloc in ['static', 'dynamic']:
    for flttype in ['float', 'double']:
        image_name = machine + '_' + alloc + '_' + flttype + '.png'

        kernel1 = summary.kernel_data(alloc, flttype, 1)
        kernel2 = summary.kernel_data(alloc, flttype, 2)
        kernel3 = summary.kernel_data(alloc, flttype, 3)
        kernel5 = summary.kernel_data(alloc, flttype, 5)
        kernel6 = summary.kernel_data(alloc, flttype, 6)

        if plotFPPeak:
            kernel1 = percent_fp_peak(kernel1, machine, flttype)
            kernel2 = percent_fp_peak(kernel2, machine, flttype)
            kernel3 = percent_fp_peak(kernel3, machine, flttype)
            kernel5 = percent_fp_peak(kernel5, machine, flttype)
            kernel6 = percent_fp_peak(kernel6, machine, flttype)

        fig = plt.figure()
        ax = fig.add_subplot(111)

        ax.plot(kernel1[:,0],kernel1[:,1], 'rv-', ms=14, lw=1.5, label='Explicitly inlined')
        ax.errorbar(kernel1[:,0], kernel1[:,1], yerr=kernel1[:,2], color='r', label='', capsize=10, lw=1.5, mew=2)

        ax.plot(kernel2[:,0],kernel2[:,1], 'g*-', ms=14, lw=1.5, label='Same file')
        ax.errorbar(kernel2[:,0], kernel2[:,1], yerr=kernel2[:,2], color='g', label='', capsize=10, lw=1.5, mew=2)

        ax.plot(kernel3[:,0],kernel3[:,1], 'b^-', ms=14, lw=1.5, label='Same file + restrict')
        ax.errorbar(kernel3[:,0], kernel3[:,1], yerr=kernel3[:,2], color='b', label='', capsize=10, lw=1.5, mew=2)

        ax.plot(kernel5[:,0],kernel5[:,1], 'm+-', ms=14, lw=1.5, mew=2, label='Diff file')
        ax.errorbar(kernel5[:,0], kernel5[:,1], yerr=kernel5[:,2], color='m', label='', capsize=10, lw=1.5, mew=2)

        ax.plot(kernel6[:,0],kernel6[:,1], 'yo-', ms=14, lw=1.5, label='Diff file + restrict')
        ax.errorbar(kernel6[:,0], kernel6[:,1], yerr=kernel6[:,2], color='y', label='', capsize=10, lw=1.5, mew=2)

        ax.set_xlabel(("Inner loop size"))
        ax.set_xscale('log', basex=2)
        ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        ax.set_xticks([32,128,512])
        ax.set_xticklabels(['32','128','512'])

        # Extend the axis so that the data points do not get cut off
        xl,xu = ax.get_xlim()
        factor = 16.0
        newxl = xl - (xl/factor)
        newxu = xu + (xu/factor)
        ax.set_xlim((newxl,newxu))

        if plotFPPeak:
            ax.set_ylabel(("Percentage of FP peak"))
            ax.set_ylim((0, 50))
        else:
            ax.set_ylabel(("GFLOP / s"))

        ax.legend(loc='upper right')
        ax.grid(True)

        fig.savefig(image_name)
