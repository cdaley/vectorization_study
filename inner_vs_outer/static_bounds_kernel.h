#ifndef STATIC_BOUNDS_KERNEL_H
#define STATIC_BOUNDS_KERNEL_H

#include "base.h"

void static_bounds_kernel(real * x,
			  real * y,
			  real a);

void static_bounds_kernel_restrict(real * __restrict__ x,
				   real * __restrict__ y,
				   real a);

void static_bounds_kernel_align_value(real_ptr x,
				      real_ptr y,
				      real a);

#endif
