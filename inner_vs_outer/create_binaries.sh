#!/bin/bash
set -e

EXE=inner_vs_outer
VERSION="$(git rev-parse --short HEAD)"

# Keep outercounts below 2147483647 (maximum signed integer)!

types=("float" "double")
allocations=("static" "dynamic")

# For my laptop
# innercounts=("32" "128" "512" "33554432")
# outercounts=("536870912" "134217728" "33554432" "64")
# basedef="-DMAXOPENMP=2"

# For Edison
# innercounts=("32" "128" "512" "2097152")
# outercounts=("536870912" "134217728" "33554432" "1024")
# basedef="-DMAXOPENMP=48"

# For Mira
innercounts=("32" "128" "512" "2097152")
outercounts=("268435456" "67108864" "16777216" "512")
basedef="-DMAXOPENMP=64"


make clean
for dtype in ${types[@]}; do
    if [ "$dtype" == "float" ]; then
	typedef=""
    else
	typedef="-DUSE_DOUBLE"
    fi


    for alloc in ${allocations[@]}; do
	if [ "$alloc" == "static" ]; then
	    allocdef=""
	else
	    allocdef="-DDYNAMIC_ALLOC"
	fi


	for (( i = 0 ; i < ${#innercounts[@]} ; i++ )); do
	    innercount="${innercounts[$i]}"
	    outercount="${outercounts[$i]}"
	    loopdef="-DLOOP_COUNT=${innercount} -DMAXFLOPS_ITERS=${outercount}"

	    def="${basedef} ${typedef} ${allocdef} ${loopdef}"
	    rundir="runs/${VERSION}/${dtype}/${alloc}/${innercount}"
	    mkdir -p "${rundir}"

	    make "CDEFINES=${def}"
	    cp "${EXE}" "${rundir}/"
	    make clean
	done
    done
done
