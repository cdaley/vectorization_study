#ifndef INNER_VS_OUTER_H
#define INNER_VS_OUTER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include <sys/time.h>

#include "base.h"
#include "static_bounds_kernel.h"

double dtime();

void internal_static_bounds_kernel(real * x,
				   real * y,
				   real a);

void internal_static_bounds_kernel_restrict(real * __restrict__ x,
					    real * __restrict__ y,
					    real a);

void internal_static_bounds_kernel_align_value(real_ptr x,
					       real_ptr y,
					       real a);

#endif
