//
//
// helloflops3
//
// A simple example that gets lots of Flops (Floating Point Operations) on
// Intel(r) Xeon Phi(tm) co-processors using openmp to scale
//

#include "inner_vs_outer.h"

const char * const experiments[] =
{
  "Kernel explicitly inlined",
  "Kernel in a function in the same source file",
  "Kernel in a function in the same source file with restrict",
  "Kernel in a function in the same source file with restrict and align_value",
  "Kernel in a function in a different source file",
  "Kernel in a function in a different source file with restrict",
  "Kernel in a function in a different source file with restrict and align_value"
};


#ifndef DYNAMIC_ALLOC
# if defined(__ICC) || defined(__INTEL_COMPILER)
// define some arrays -
// make sure they are 64 byte aligned
// for best cache access
static real x[FLOPS_ARRAY_SIZE] __attribute__((align(64)));
static real y[FLOPS_ARRAY_SIZE] __attribute__((align(64)));
# else
// Valid with IBM and GNU compilers
// Only need 32 byte alignment for BG/Q, but choose 128 byte alignment to match L2
// cache line size: L1 = 64 bytes, L2 = 128 bytes
static real x[FLOPS_ARRAY_SIZE] __attribute__((aligned(128)));
static real y[FLOPS_ARRAY_SIZE] __attribute__((aligned(128)));
# endif
#endif

// Main program - pedal to the metal...calculate using tons o'flops!
//
int main(int argc, char *argv[])
{
    double tstart, tstop, ttime;
    double gflops;
    double bytes_per_thread;
    const real a=1.1;
    const int num_experiments = sizeof(experiments)/sizeof(*experiments);
    int offset, i, j, k, l, numthreads;
#ifdef DYNAMIC_ALLOC
    real *x, *y;
# if defined(__IBMC__) || defined(__IBMCPP__)
#pragma disjoint(*x, *y)
# endif
#endif

#ifdef _OPENMP
#pragma omp parallel
#pragma omp master
    numthreads = omp_get_num_threads();
#endif

    if (numthreads > MAXOPENMP) {
      printf("ERROR! numthreads=%d, MAXOPENMP=%d\n", numthreads, MAXOPENMP);
      exit(EXIT_FAILURE);
    }


#ifdef DYNAMIC_ALLOC
    // Allocate the same amount of memory as we would with static arrays
    // We could allocate less, but then the test would be different.
# if defined(__ICC) || defined(__INTEL_COMPILER)
    printf("Arrays are dynamically allocated with _mm_malloc\n");
    x = _mm_malloc(sizeof(real)*FLOPS_ARRAY_SIZE, 64);
    y = _mm_malloc(sizeof(real)*FLOPS_ARRAY_SIZE, 64);
# else
    // Valid with IBM and GNU compilers
    printf("Arrays are dynamically allocated with posix_memalign\n");
    posix_memalign((void**)&x, 128, sizeof(real)*FLOPS_ARRAY_SIZE);
    posix_memalign((void**)&y, 128, sizeof(real)*FLOPS_ARRAY_SIZE);
#  if defined(__IBMC__) || defined(__IBMCPP__)
    __alignx(32, x);
    __alignx(32, y);
#  elif defined(__GNUC__) || defined(__GNUG__)
    __builtin_assume_aligned(x, 64);
    __builtin_assume_aligned(y, 64);
#  endif
# endif
#else
    printf("Arrays are statically allocated\n");
#endif

    printf("Floating point size = %zu\n", sizeof(real));
    printf("Inner loop size = %d\n", LOOP_COUNT);
    printf("Outer loop size = %d\n", MAXFLOPS_ITERS);
    printf("Maximum OpenMP threads = %d\n", MAXOPENMP);
    printf("Actual OpenMP threads = %d\n", numthreads);

    bytes_per_thread = 2.0 * sizeof(real) * LOOP_COUNT;
    printf("Memory accessed per thread = %.2lf MiB, %.2lf KiB, %.0lf bytes\n",
	   bytes_per_thread / (1024.0 * 1024.0),
	   bytes_per_thread / (1024.0),
	   bytes_per_thread);
    printf("Total memory allocation = %.2lf MiB\n",
	   bytes_per_thread * MAXOPENMP / (1024.0 * 1024.0));


    for(l=0; l<num_experiments; ++l) {

    //
    // initialize the compute arrays
    //
    //
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for(i=0; i<FLOPS_ARRAY_SIZE; i++)
    {
        x[i] = (real)i + 0.1;
        y[i] = (real)i + 0.2;
    }
    printf("\nWill run kernel %d with %d threads\n", l, numthreads);
    printf("%s\n", experiments[l]);

    tstart = dtime();

    // scale the calculation across threads requested
    // need to set environment variables OMP_NUM_THREADS and KMP_AFFINITY
#ifdef _OPENMP
#pragma omp parallel for private(j,k)
#endif
    for (i=0; i<numthreads; i++)
    {
        // each thread will work it's own array section
        // calc offset into the right section
        offset = i*LOOP_COUNT;
	switch(l)
	{
	case 0:
	  // loop many times to get lots of calculations
	  for(j=0; j<MAXFLOPS_ITERS; j++)
	  {
	    // scale 1st array and add in the 2nd array
#if defined(__ICC) || defined(__INTEL_COMPILER)
#pragma simd
#pragma vector aligned
#elif defined(__IBMC__) || defined(__IBMCPP__)
#pragma ibm independent_loop
#endif
	    for(k=0; k<LOOP_COUNT; k++)
	    {
	      x[k+offset] = a * x[k+offset] + y[k+offset];
	    }
	  }
	  break;
	case 1:
	  internal_static_bounds_kernel(&x[offset], &y[offset], a);
	  break;
	case 2:
	  internal_static_bounds_kernel_restrict(&x[offset], &y[offset], a);
	  break;
	case 3:
	  internal_static_bounds_kernel_align_value(&x[offset], &y[offset], a);
	  break;
	case 4:
	  static_bounds_kernel(&x[offset], &y[offset], a);
	  break;
	case 5:
	  static_bounds_kernel_restrict(&x[offset], &y[offset], a);
	  break;
	case 6:
	  static_bounds_kernel_align_value(&x[offset], &y[offset], a);
	  break;
	default:
	  break;
	}
    }

    tstop = dtime();
    // # of gigaflops we just calculated
    gflops = (double)( 1.0e-9*numthreads*LOOP_COUNT*
                        MAXFLOPS_ITERS*FLOPSPERCALC);

    //elasped time
    ttime = tstop - tstart;
    //
    // Print the results
    //
    if ((ttime) > 0.0)
    {
        printf("GFlops = %10.3lf, Secs = %10.3lf, GFlops per sec = %10.3lf\n",
	       gflops, ttime, gflops/ttime);
    }

    } // End of custom loop over experiments

#ifdef DYNAMIC_ALLOC
# if defined(__ICC) || defined(__INTEL_COMPILER)
    _mm_free(x);
    _mm_free(y);
# else
    free(x);
    free(y);
# endif
#endif

    return 0;
}


// dtime
//
// returns the current wall clock time
//
double dtime()
{
    double tseconds = 0.0;
    struct timeval mytime;
    gettimeofday(&mytime,(struct timezone*)0);
    tseconds = (double)(mytime.tv_sec + mytime.tv_usec*1.0e-6);
    return tseconds;
}



void internal_static_bounds_kernel(real * x,
				   real * y,
				   real a)
{
  int j, k;
#if defined(__ICC) || defined(__INTEL_COMPILER)
#elif defined(__IBMC__) || defined(__IBMCPP__)
#pragma disjoint(*x, *y)
  __alignx(32, x);
  __alignx(32, y);
#elif defined(__GNUC__) || defined(__GNUG__)
  __builtin_assume_aligned(x, 64);
  __builtin_assume_aligned(y, 64);
#endif

  // loop many times to get lots of calculations
  for(j=0; j<MAXFLOPS_ITERS; j++)
  {
    // scale 1st array and add in the 2nd array
#if defined(__ICC) || defined(__INTEL_COMPILER)
#pragma simd
#pragma vector aligned
#elif defined(__IBMC__) || defined(__IBMCPP__)
#pragma ibm independent_loop
#endif
    for(k=0; k<LOOP_COUNT; k++)
    {
      x[k] = a * x[k] + y[k];
    }
  }
}


void internal_static_bounds_kernel_restrict(real * __restrict__ x,
					    real * __restrict__ y,
					    real a)
{
  int j, k;
#if defined(__ICC) || defined(__INTEL_COMPILER)
#elif defined(__IBMC__) || defined(__IBMCPP__)
  __alignx(32, x);
  __alignx(32, y);
#elif defined(__GNUC__) || defined(__GNUG__)
  __builtin_assume_aligned(x, 64);
  __builtin_assume_aligned(y, 64);
#endif

  // loop many times to get lots of calculations
  for(j=0; j<MAXFLOPS_ITERS; j++)
  {
    // scale 1st array and add in the 2nd array
#if defined(__ICC) || defined(__INTEL_COMPILER)
#pragma simd
#pragma vector aligned
#elif defined(__IBMC__) || defined(__IBMCPP__)
#pragma ibm independent_loop
#endif
    for(k=0; k<LOOP_COUNT; k++)
    {
      x[k] = a * x[k] + y[k];
    }
  }
}


void internal_static_bounds_kernel_align_value(real_ptr x,
					       real_ptr y,
					       real a)
{
  int j, k;
#if defined(__ICC) || defined(__INTEL_COMPILER)
#elif defined(__IBMC__) || defined(__IBMCPP__)
  __alignx(32, x);
  __alignx(64, y);
#elif defined(__GNUC__) || defined(__GNUG__)
  __builtin_assume_aligned(x, 64);
  __builtin_assume_aligned(y, 64);
#endif

  // loop many times to get lots of calculations
  for(j=0; j<MAXFLOPS_ITERS; j++)
  {
    // scale 1st array and add in the 2nd array
#if defined(__ICC) || defined(__INTEL_COMPILER)
#pragma simd
#pragma vector aligned
#elif defined(__IBMC__) || defined(__IBMCPP__)
#pragma ibm independent_loop
#endif
    for(k=0; k<LOOP_COUNT; k++)
    {
      x[k] = a * x[k] + y[k];
    }
  }
}
