#!/bin/bash -x
#
# Asssumptions:
#
#  * this script is named cobalt_job.sh.
#  * the job launch script is named qsub.sh.
#  * the current directory is empty except for the two scripts above.
#  * the flash4 binary and template flash.par are in the parent directory.
#
# Notes
#  * COBALT_JOBSIZE shows the number of nodes requested in the qsub line
#  * LOCARGS allows us to use this script for sub-block jobs (< 128 nodes) too

PROG="inner_vs_outer"
NODES=${COBALT_JOBSIZE}
RANKS_PER_NODE=1
OMP_NUM_THREADS_ARR=( "16" "32" "64" )

NPROCS=$((NODES*RANKS_PER_NODE))
LOCARGS="--block $COBALT_PARTNAME ${COBALT_CORNER:+--corner} $COBALT_CORNER ${COBALT_SHAPE:+--shape} $COBALT_SHAPE"

for ((i=0; i < ${#OMP_NUM_THREADS_ARR[@]}; i++)); do

    OMP_NUM_THREADS=${OMP_NUM_THREADS_ARR[$i]}

    # BG_THREADLAYOUT:  1 - default next core first; 2 - my core first
    runjob \
	--ranks-per-node ${RANKS_PER_NODE} \
	--np ${NPROCS} \
	--cwd ${PWD} \
	${LOCARGS} \
	--envs BG_THREADLAYOUT=2 \
	--envs BG_SHAREDMEMSIZE=32 \
	--envs BG_COREDUMPONERROR=1 \
	--envs OMP_NUM_THREADS=${OMP_NUM_THREADS} \
	--envs OMP_STACKSIZE=32M \
	--envs L1P_POLICY=std \
	--envs PAMID_VERBOSE=1 \
	--envs LANG=en_US \
	--envs NLSPATH=${IBM_MAIN_DIR}/msg/bg/%L/%N \
	: ${PROG}
    echo "runjob return code is " $?
    cd ..
done

exit 0
