#ifndef BASE_H
#define BASE_H

/* Number of iterations in the inner loop */
#ifndef LOOP_COUNT
# define LOOP_COUNT 128
#endif

/* Number of iterations in the outer loop */
#ifndef MAXFLOPS_ITERS
# define MAXFLOPS_ITERS 20000000
#endif

/* Maximum number of OpenMP threads */
#ifndef MAXOPENMP
# define MAXOPENMP 1024
#endif

#define FLOPS_ARRAY_SIZE (LOOP_COUNT*MAXOPENMP)

/* number of float pt ops per calculation */
#define FLOPSPERCALC 2

#ifdef USE_DOUBLE
typedef double real;
#else
typedef float real;
#endif

typedef real * __restrict__  __attribute__((align_value(64))) real_ptr;

#endif
