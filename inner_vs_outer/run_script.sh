#!/bin/bash
set -e

EXE=inner_vs_outer
VERSION="$(git rev-parse --short HEAD)"

types=("float" "double")
allocations=("static" "dynamic")
loopcounts=("32" "128" "512" "33554432")

for dtype in ${types[@]}; do
    for alloc in ${allocations[@]}; do
	for loopcount in ${loopcounts[@]}; do
	    rundir="runs/${VERSION}/${dtype}/${alloc}/${loopcount}"
	    ./"${rundir}/$EXE"
	done
    done
done
