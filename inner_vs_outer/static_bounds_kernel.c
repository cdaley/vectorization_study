#include "static_bounds_kernel.h"

void static_bounds_kernel(real * x,
			  real * y,
			  real a)
{
  int j, k;
#if defined(__ICC) || defined(__INTEL_COMPILER)
#elif defined(__IBMC__) || defined(__IBMCPP__)
#pragma disjoint(*x, *y)
  __alignx(32, x);
  __alignx(32, y);
#elif defined(__GNUC__) || defined(__GNUG__)
  __builtin_assume_aligned(x, 64);
  __builtin_assume_aligned(y, 64);
#endif

  // loop many times to get lots of calculations
  for(j=0; j<MAXFLOPS_ITERS; j++)
  {
    // scale 1st array and add in the 2nd array
#if defined(__ICC) || defined(__INTEL_COMPILER)
#pragma simd
#pragma vector aligned
#elif defined(__IBMC__) || defined(__IBMCPP__)
#pragma ibm independent_loop
#endif
    for(k=0; k<LOOP_COUNT; k++)
    {
      x[k] = a * x[k] + y[k];
    }
  }
}


void static_bounds_kernel_restrict(real * __restrict__ x,
				   real * __restrict__ y,
				   real a)
{
  int j, k;
#if defined(__ICC) || defined(__INTEL_COMPILER)
#elif defined(__IBMC__) || defined(__IBMCPP__)
  __alignx(32, x);
  __alignx(32, y);
#elif defined(__GNUC__) || defined(__GNUG__)
  __builtin_assume_aligned(x, 64);
  __builtin_assume_aligned(y, 64);
#endif

  // loop many times to get lots of calculations
  for(j=0; j<MAXFLOPS_ITERS; j++)
  {
    // scale 1st array and add in the 2nd array
#if defined(__ICC) || defined(__INTEL_COMPILER)
#pragma simd
#pragma vector aligned
#elif defined(__IBMC__) || defined(__IBMCPP__)
#pragma ibm independent_loop
#endif
    for(k=0; k<LOOP_COUNT; k++)
    {
      x[k] = a * x[k] + y[k];
    }
  }
}


void static_bounds_kernel_align_value(real_ptr x,
				      real_ptr y,
				      real a)
{
  int j, k;
#if defined(__ICC) || defined(__INTEL_COMPILER)
#elif defined(__IBMC__) || defined(__IBMCPP__)
  __alignx(32, x);
  __alignx(32, y);
#elif defined(__GNUC__) || defined(__GNUG__)
  __builtin_assume_aligned(x, 64);
  __builtin_assume_aligned(y, 64);
#endif

  // loop many times to get lots of calculations
  for(j=0; j<MAXFLOPS_ITERS; j++)
  {
    // scale 1st array and add in the 2nd array
#if defined(__ICC) || defined(__INTEL_COMPILER)
#pragma simd
#pragma vector aligned
#elif defined(__IBMC__) || defined(__IBMCPP__)
#pragma ibm independent_loop
#endif
    for(k=0; k<LOOP_COUNT; k++)
    {
      x[k] = a * x[k] + y[k];
    }
  }
}
