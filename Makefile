# Dyanmic allocation can slow the performance
#CDEFINES = -DDYNAMIC_ALLOC

CC = icc
CFLAGS = $(CDEFINES) -openmp -O3 -vec-report=6 -mmic
#CC = gcc
#CFLAGS = $(CDEFINES) -fopenmp -Ofast -msse4.2

all: helloflops3_xphi

helloflops3.o : helloflops3.c helloflops3.h static_bounds_kernel.h constants.h
	$(CC) $(CFLAGS) helloflops3.c -S
	$(CC) $(CFLAGS) helloflops3.c -c

static_bounds_kernel.o: static_bounds_kernel.c static_bounds_kernel.h constants.h
	$(CC) $(CFLAGS) static_bounds_kernel.c -S
	$(CC) $(CFLAGS) static_bounds_kernel.c -c

dynamic_bounds_kernel.o: dynamic_bounds_kernel.c dynamic_bounds_kernel.h constants.h
	$(CC) $(CFLAGS) dynamic_bounds_kernel.c -S
	$(CC) $(CFLAGS) dynamic_bounds_kernel.c -c

helloflops3_xphi : static_bounds_kernel.o dynamic_bounds_kernel.o helloflops3.o
	$(CC) $(CFLAGS) -o helloflops3_xphi static_bounds_kernel.o dynamic_bounds_kernel.o helloflops3.o

clean: 
	rm -f *_xphi *.o *.s
