#include "static_bounds_kernel.h"

void static_bounds_kernel(float * const x,
			  const float * const y,
			  const float a)
{
  int j, k;

  // loop many times to get lots of calculations
  for(j=0; j<MAXFLOPS_ITERS; j++)
  {
    // scale 1st array and add in the 2nd array
#pragma simd
#pragma vector aligned
    for(k=0; k<LOOP_COUNT; k++)
    {
      x[k] = a * x[k] + y[k];
    }
  }
}


void static_bounds_kernel_value_att(float_ptr const x,
				    const float_ptr const y,
				    const float a)
{
  int j, k;

  // loop many times to get lots of calculations
  for(j=0; j<MAXFLOPS_ITERS; j++)
  {
    // scale 1st array and add in the 2nd array
#pragma simd
#pragma vector aligned
    for(k=0; k<LOOP_COUNT; k++)
    {
      x[k] = a * x[k] + y[k];
    }
  }
}
