#!/bin/bash
set -e

EXE=helloflops3_xphi
VERSION="$(git rev-parse --short HEAD)"


make clean
make
rundir="runs/${VERSION}/static"
mkdir -p "${rundir}"
cp batch_submission_script "${EXE}" "${rundir}/"


make clean
make CDEFINES=-DDYNAMIC_ALLOC
rundir="runs/${VERSION}/dynamic"
mkdir -p "${rundir}"
cp batch_submission_script "${EXE}" "${rundir}/"

make clean
