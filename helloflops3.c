//
//
// helloflops3
//
// A simple example that gets lots of Flops (Floating Point Operations) on
// Intel(r) Xeon Phi(tm) co-processors using openmp to scale
//

#include "helloflops3.h"

const char * const experiments[] =
{
  "Kernel explicitly inlined",
  "Kernel in a function in the same source file",
  "Kernel in a function in a different source file",
  "Kernel in a function in a different source file with aligned value attibute",
  "Kernel in a function in the same source file with outer loop bounds determined at runtime",
  "Kernel in a function in the same source file with inner and outer loop bounds determined at runtime",
  "Kernel in a function in a different source file with inner and outer loop bounds determined at runtime"
};


#ifndef DYNAMIC_ALLOC
// define some arrays -
// make sure they are 64 byte aligned
// for best cache access
//
static float fa[FLOPS_ARRAY_SIZE] __attribute__((align(64)));
static float fb[FLOPS_ARRAY_SIZE] __attribute__((align(64)));
#endif

// Main program - pedal to the metal...calculate using tons o'flops!
//
int main(int argc, char *argv[])
{
    int i,j,k,l;
    int numthreads;
    double tstart, tstop, ttime;
    double gflops = 0.0;
    float a=1.1;

#ifdef DYNAMIC_ALLOC
    float *fa __attribute__((align(64))), *fb __attribute__((align(64)));
# ifdef __MIC__
    printf("Arrays are dynamically allocated with _mm_malloc\n");
    fa = _mm_malloc(sizeof(float)*FLOPS_ARRAY_SIZE, 64);
    fb = _mm_malloc(sizeof(float)*FLOPS_ARRAY_SIZE, 64);
# else
    printf("Arrays are dynamically allocated with malloc\n");
    fa = malloc(sizeof(float)*FLOPS_ARRAY_SIZE);
    fb = malloc(sizeof(float)*FLOPS_ARRAY_SIZE);
# endif

#else
    printf("Arrays are statically allocated\n");
#endif

    //
    // initialize the compute arrays
    //
    //

#pragma omp parallel
#pragma omp master
    numthreads = omp_get_num_threads();


    for(l=0; l<7; ++l) {

#pragma omp parallel for
    for(i=0; i<FLOPS_ARRAY_SIZE; i++)
    {
        fa[i] = (float)i + 0.1;
        fb[i] = (float)i + 0.2;
    }
    printf("\nWill run kernel %d with %d threads\n", l, numthreads);
    printf("%s\n", experiments[l]);

    tstart = dtime();

    // scale the calculation across threads requested
    // need to set environment variables OMP_NUM_THREADS and KMP_AFFINITY

#pragma omp parallel for private(j,k)
    for (i=0; i<numthreads; i++)
    {
        // each thread will work it's own array section
        // calc offset into the right section
        int offset = i*LOOP_COUNT;
	select_kernel(fa, fb, a, offset, l);
    }

    tstop = dtime();
    // # of gigaflops we just calculated
    gflops = (double)( 1.0e-9*numthreads*LOOP_COUNT*
                        MAXFLOPS_ITERS*FLOPSPERCALC);

    //elasped time
    ttime = tstop - tstart;
    //
    // Print the results
    //
    if ((ttime) > 0.0)
    {
        printf("GFlops = %10.3lf, Secs = %10.3lf, GFlops per sec = %10.3lf\r\n",
	       gflops, ttime, gflops/ttime);
    }

    } // End of custom loop over experiments

#ifdef DYNAMIC_ALLOC
# ifdef __MIC__
    _mm_free(fa);
    _mm_free(fb);
# else
    free(fa);
    free(fb);
# endif
#endif

    return 0;
}


// dtime
//
// returns the current wall clock time
//
double dtime()
{
    double tseconds = 0.0;
    struct timeval mytime;
    gettimeofday(&mytime,(struct timezone*)0);
    tseconds = (double)(mytime.tv_sec + mytime.tv_usec*1.0e-6);
    return tseconds;
}


void select_kernel(float * const x,
		   const float * const y,
		   const float a,
		   const int offset,
		   const int l)
{
  int j, k;

  switch(l)
  {
  case 0:
    // loop many times to get lots of calculations
    for(j=0; j<MAXFLOPS_ITERS; j++)
    {
      // scale 1st array and add in the 2nd array
#pragma simd
#pragma vector aligned
      for(k=0; k<LOOP_COUNT; k++)
      {
	x[k+offset] = a * x[k+offset] + y[k+offset];
      }
    }
    break;
  case 1:
    internal_static_bounds_kernel(&x[offset], &y[offset], a);
    break;
  case 2:
    static_bounds_kernel(&x[offset], &y[offset], a);
    break;
  case 3:
    static_bounds_kernel_value_att(&x[offset], &y[offset], a);
    break;
  case 4:
    internal_dynamic_outer_bound_kernel(&x[offset], &y[offset], a, MAXFLOPS_ITERS);
    break;
  case 5:
    internal_dynamic_bounds_kernel(&x[offset], &y[offset], a, MAXFLOPS_ITERS, LOOP_COUNT);
    break;
  case 6:
    dynamic_bounds_kernel(&x[offset], &y[offset], a, MAXFLOPS_ITERS, LOOP_COUNT);
    break;
  default:
    break;
  }
}


void internal_static_bounds_kernel(float * const x,
				   const float * const y,
				   const float a)
{
  int j, k;

  // loop many times to get lots of calculations
  for(j=0; j<MAXFLOPS_ITERS; j++)
  {
    // scale 1st array and add in the 2nd array
#pragma simd
#pragma vector aligned
    for(k=0; k<LOOP_COUNT; k++)
    {
      x[k] = a * x[k] + y[k];
    }
  }
}


void internal_dynamic_outer_bound_kernel(float * const x,
					 const float * const y,
					 const float a,
					 const int iters)
{
  int j, k;

  // loop many times to get lots of calculations
  for(j=0; j<iters; j++)
  {
    // scale 1st array and add in the 2nd array
#pragma simd
#pragma vector aligned
    for(k=0; k<LOOP_COUNT; k++)
    {
      x[k] = a * x[k] + y[k];
    }
  }
}


void internal_dynamic_bounds_kernel(float * const x,
				    const float * const y,
				    const float a,
				    const int iters,
				    const int cnt)
{
  int j, k;

  // loop many times to get lots of calculations
  for(j=0; j<iters; j++)
  {
    // scale 1st array and add in the 2nd array
#pragma simd
#pragma vector aligned
    for(k=0; k<cnt; k++)
    {
      x[k] = a * x[k] + y[k];
    }
  }
}
