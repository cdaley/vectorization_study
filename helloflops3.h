#ifndef HELLOFLOPS3_H
#define HELLOFLOPS3_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include <sys/time.h>

#include "constants.h"
#include "static_bounds_kernel.h"
#include "dynamic_bounds_kernel.h"

double dtime();


void select_kernel(float * const x,
		   const float * const y,
		   const float a,
		   const int offset,
		   const int l);


void internal_static_bounds_kernel(float * const x,
				   const float * const y,
				   const float a);


void internal_dynamic_outer_bound_kernel(float * const x,
					 const float * const y,
					 const float a,
					 const int iters);


void internal_dynamic_bounds_kernel(float * const x,
				    const float * const y,
				    const float a,
				    const int iters,
				    const int cnt);

#endif
